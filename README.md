# Piwosz Serwer-Side

Aplikacja serwera z wykorzystaniem technologi Spring Boot na potrzeby aplikacji "Piwosz". 

## Dokumentacja

Jako, że projekt jest komercyjny, kod nie jest udostępniany.
Jednak do potrzebnej współpracy z serwerem dostępna jest dokumentacja API. 

[Dokumentacja](https://piwosz.azurewebsites.net/swagger-ui.html) - swagger-ui

![swagger doc](img/swagger.png "doc")

### Uwaga

Projekt w fazie testowej, serwer może włączać się około minuty, przy pierwszym request-cie.

## Aplikacja mobilna

[Piwosz App](https://play.google.com/apps/testing/com.beerio) 

![app-image](img/app-image.jpg "app")
